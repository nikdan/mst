#include <string>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <cstdlib>
#include <cmath>
#include <vector>

const size_t max_weight = 20;
const size_t zero_proc = 30;
int dim = 5;


inline void clear_cross (int i, int j, std::vector< std::vector<int> >& arr)
{
	for (int k = 0; k < dim; ++k) {
		arr[i][k] = arr[k][j] = 0;
	}
}


inline int gen_rand()
{
	int main_rand = rand() % max_weight;
	int rand_zero = (rand() % 100) / zero_proc;
	return rand_zero == 0 ? 0 : main_rand;
}


std::vector< std::vector<int> > fill ()
{
	std::vector< std::vector<int> > arr;
	arr.resize (dim);
	for (int i = 0; i < dim; ++i)
	{
		arr[i].resize (dim);
		for (int j = 0; j <= i; ++j)
		{
			int new_el (i==j ? 0 : gen_rand());
			arr[i][j] = new_el;
			arr[j][i] = arr[i][j];
		}
	}
	return arr;
}


int rude_min_weight (std::vector< std::vector<int> >& arr)
{
	int summ_way (0);
	for (int i = 0; i < dim; ++i)
	{
		float min_in_str (INFINITY);
		int min_col;
		for (int j = 0; j < dim; ++j)
		{
			if ((arr[i][j] > 0) && (arr[i][j] < min_in_str))
			{
				min_in_str = arr[i][j];
				min_col = j;
			}
		}
		arr[min_col][i] = arr[i][min_col] = 0;
		if (min_in_str <= max_weight) {
			summ_way += min_in_str;
		}
	}
	return summ_way;
}


void print_table (std::ofstream& out, std::vector< std::vector<int> >& arr)
{
	for (int i = 0; i < dim; ++i)
	{
		for (int j = 0; j < dim; ++j)
		{
			out << std::setw(4) << arr[i][j];
		}
		out << '\n';
	}
}


int main (int argc, char *argv[])
{
	std::string filename ("in_5.dat");
	if (argc > 1)
	{
		filename = argv[1];
		dim = atoi (argv[2]);
	}
	std::ofstream out ( filename.c_str() );
	std::vector< std::vector<int> > arr = fill();

	out << std::setw(4) << dim << '\n';
	print_table (out, arr);
	out << std::setw(4) << rude_min_weight(arr) << '\n';
}
