#ifndef TREE_HPP
#define TREE_HPP

#include <vector>
#include <memory>
#include <map>
#include <set>


class node
{
	int id;

public:
	node () : id {-1} {}
	node (int id) : id {id} {}
	int get_id() const {return id;}
};
bool operator < (const node& first, const node& second);
bool operator == (const node& first, const node& second);


class edge
{
	double my_weight;
	std::pair<node, node> my_nodes;

public:
	edge ();
	edge (node first, node second, double weight = 0);
	double get_wght() const { return my_weight; }
	node  get_fnode() const { return my_nodes.first; }
	node  get_snode() const { return my_nodes.second;}
	bool  is_near (node first, node second) const;
	bool  is_near (node other) const;
};
bool operator < (const edge& first, const edge& second);
bool operator == (const edge& first, const edge& second);


typedef std::shared_ptr< std::multiset<edge> >  ptr_edges;

struct graph : public std::map<node, ptr_edges>
{
	int rude_min_weight;
};

graph gen_graph (const std::string& filename);

ptr_edges Prim (graph& extrn);

void print_tree ( const ptr_edges& mst );

#endif
